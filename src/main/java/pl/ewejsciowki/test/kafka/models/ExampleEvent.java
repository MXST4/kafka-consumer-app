package pl.ewejsciowki.test.kafka.models;

import lombok.Builder;
import lombok.Value;

import java.time.LocalDateTime;

@Builder
@Value
public class ExampleEvent {

    String id;

    LocalDateTime createdAt;

    AdditionalInfo additionalInfo;

    @Value
    @Builder
    public static class AdditionalInfo {

        String property1;

        String property2;

    }

}
