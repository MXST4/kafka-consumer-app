package pl.ewejsciowki.test.kafka.producer;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "kafka_outgoing_messages ")
@Data
public class KafkaOutgoingMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String topic;

    private String payload;

}
