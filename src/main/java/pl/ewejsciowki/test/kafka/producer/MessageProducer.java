package pl.ewejsciowki.test.kafka.producer;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import pl.ewejsciowki.test.kafka.models.ExampleEvent;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class MessageProducer {

    private final KafkaOutgoingMessageRepository repository;

    private final ObjectMapper objectMapper;

    @Scheduled(fixedDelay = 1000)
    public void periodicallySendKafkaOutgoingMessage() {
        var kafkaOutgoingMessage = new KafkaOutgoingMessage();

        kafkaOutgoingMessage.setTopic("reservationUpdated");
        kafkaOutgoingMessage.setPayload(givenExampleEvent());

        repository.save(kafkaOutgoingMessage);
    }

    @SneakyThrows
    private String givenExampleEvent() {
        var exampleEvent = ExampleEvent.builder()
                                       .id(UUID.randomUUID().toString())
                                       .createdAt(LocalDateTime.now())
                                       .additionalInfo(ExampleEvent.AdditionalInfo.builder()
                                                                                  .property1(UUID.randomUUID().toString())
                                                                                  .property2(UUID.randomUUID().toString())
                                                                                  .build())
                                       .build();

        return objectMapper.writeValueAsString(exampleEvent);
    }

}
