package pl.ewejsciowki.test.kafka.producer;

import org.springframework.data.repository.CrudRepository;

public interface KafkaOutgoingMessageRepository extends CrudRepository<KafkaOutgoingMessage, Long> {

}
